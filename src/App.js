import React, { Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faChartBar, faGlobeAmericas, faMobileAlt } from '@fortawesome/free-solid-svg-icons'
import Main from './container/MainContainer';
import Header from './container/HeaderContainer'
import './App.css';

library.add(faChartBar,faGlobeAmericas, faMobileAlt)

class App extends Component {
  render() {
    return (
      <div className="App">
          <Header />
          <Main/>
      </div>
    );
  }
}

export default App;
