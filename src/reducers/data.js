import { UPDATE_MAXIMUM, ADD_DEVICE, REMOVE_DEVICE, CHANGE_TTL } from '../actions/types';

let addDevice = (devices, max, action) => {
    let newDevice = ejectDevice(action);
    console.log(`try add ${JSON.stringify(newDevice)}`)
    let index;
    if (devices.length >= max) {
        index = findWeakDevice(devices, action)
        if (index === -1) return devices;
        devices.splice(index, 1, newDevice)
    } else {
        devices = [...devices, newDevice]
        index = devices.length - 1;
    }
    newDevice['listener'] = setTimeout(() => newDevice.remover(action.name), action.TTL * 1000)

    return devices;


    function ejectDevice(action) {
        return {
            id: action.id,
            name: action.name,
            TTL: action.TTL,
            priority: action.priority,
            coord: action.coord,
            remover: action.remover
        }
    }
    function findWeakDevice(devices, newDevice) {
        let weaks = {}
        devices.forEach((device, index) => {
            if (device.priority > newDevice.priority) {
                weaks[device.priority] = index
            }
        });
        let priority = newDevice.priority;
        let min = 5
        while (min > priority) {
            if (weaks[min]) return weaks[min]
            min--;
        }
        return -1;
    }
}
const data = (state = { max: 5, devices: [] }, action) => {

    switch (action.type) {
        case UPDATE_MAXIMUM: {
            let devices = [...state.devices];
            if (action.max < devices.length) {
                devices.sort((a, b) => a.priority - b.priority);
                while (action.max < devices.length) {
                    devices.pop();
                }
            }
            return Object.assign({}, state, { max: action.max, devices: devices })
        }

        case ADD_DEVICE:
            let devices = addDevice([...state.devices], state.max, action)
            return Object.assign({}, state, { devices })
        case REMOVE_DEVICE: {
            let devices = [...state.devices]
            let index = devices.findIndex((device) => device.name === action.name)
            devices.splice(index, index !== -1 ? 1 : 0)
            return Object.assign({}, state, { devices })
        }
        case CHANGE_TTL: {
            let devices = [...state.devices]
            let index = devices.findIndex((device) => device.name === action.name)
            console.log(action.name, devices[index])
            clearTimeout(devices[index]['listener'])
            delete devices[index]['listener']
            devices[index]['TTL'] = action.TTL;
            devices[index]['listener'] = setTimeout(() => devices[index].remover(action.name), action.TTL * 1000)
            return Object.assign({}, state, { devices })
        }
        default:
            return state;
    }
}

export default data;