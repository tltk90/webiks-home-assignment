import { SET_TABLE_FILTER } from '../actions/types';
import { FILTERS } from '../actions';

const tableFilter = (state = FILTERS.ByName, action) => {
    switch (action.type) {
        case SET_TABLE_FILTER:
            return action.filter;
        default:
            return state;
    }
}

export default tableFilter;