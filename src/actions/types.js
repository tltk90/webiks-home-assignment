export const ADD_DEVICE = "ADD_DEVICE"
export const REMOVE_DEVICE = "REMOVE_DEVICE"
export const CHANGE_TTL = "CHANGE_TTL"
export const UPDATE_MAXIMUM = "UPDATE_MAXIMUM"
export const SET_TABLE_FILTER = "SET_TABLE_FILTER"