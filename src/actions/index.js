import { randomBytes } from 'crypto';
import {ADD_DEVICE, UPDATE_MAXIMUM,SET_TABLE_FILTER, REMOVE_DEVICE, CHANGE_TTL} from './types';
let nextDeviceId = 1;
let generateDeviceName = () => {
    return randomBytes(6).toString('hex')
};
let randomTTL = () => {
    return Math.floor(Math.random() * 50) +70
}
let randomPriority = () => {
    let rand = Math.random()
    if( rand >= 0 && rand < 0.4) return 5;
    if( rand >= 0.4 && rand < 0.6) return 4;
    if(rand >= 0.6 && rand < 0.85) return 3;
    if(rand >= 0.85 && rand < 0.95) return 2
    if(rand >= 0.95 && rand <= 1) return 1;
}
let getCoord = () => {
    return {
        lat: 32.041463 + Math.random() * 0.5, 
        lng: 34.792671 + Math.random() * 0.3,
    }
}

export const addDevice = (remover) => ({
    type: ADD_DEVICE,
    name: generateDeviceName(),
    id: nextDeviceId++,
    TTL: randomTTL(),
    priority: randomPriority(),
    coord: getCoord(),
    remover: remover
})

export const updateMaximum = (max) => ({
    type: UPDATE_MAXIMUM,
    max
})

export const removeDevice = (name) => ({
    type: REMOVE_DEVICE,
    name
})

export const setFilter = filter => ({
    type: SET_TABLE_FILTER,
    filter
})

export const changeTTL = (name, ttl) => ({
    type: CHANGE_TTL,
    name: name,
    TTL: ttl
})


export const FILTERS = {
    default: "default",
    ByName: "BY_NAME",
    ByTTL: "BY_TTL",
    ByPriority: "BY_PRIORITY"
}