import React, {Component} from 'react';
const COLORS = { 1: '#021335', 2: '#016d01', 3: '#f2c100', 4: '#c35a00', 5: '#950010' }

export default class DeviceRow extends Component {
    constructor(props){
        super(props)
        this.TTL = React.createRef();
    }
    onBlur = () => {
        if(!this.TTL.current.textContent){
            this.TTL.current.textContent = 1
        }
        this.changeTTL(parseInt(this.TTL.current.textContent))

    }
    keyDown= (e) => {
        console.log(e.nativeEvent)
        let code = e.nativeEvent.code
        
        if(!["NumpadEnter", "Enter", "Digit", "Backspace"].some( (c) => code.startsWith(c))){
            return e.preventDefault();
        }
        let td= this.TTL.current;
        if(code.indexOf("Enter") >= 0){
            this.changeTTL(parseInt(td.textContent))
            td.blur();
        }
    }
    changeTTL = (ttl) =>{
        this.props.changeTTL(this.props.device.name, ttl)
    }
    render(){
        const {index, device} = this.props;
        return (
            <tr style={{color: COLORS[device.priority]}}>
                <td  style={{width: '5%'}} >{index + 1}</td>
                <td style={{width: '55%'}} >{device.name}</td>
                <td style={{width: '20%'}} >{device.priority}</td>
                <td  style={{width: '20%',maxWidth: '50px', lineBreak: 'loose'}}
                ref={this.TTL}
                contentEditable="true"
                suppressContentEditableWarning="true"
                onKeyDown={this.keyDown}
                onBlur={this.onBlur}>
                    {device.TTL}
                </td>
            </tr>
        )
    }
}

