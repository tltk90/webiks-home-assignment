import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Chart from '../container/ChartContainer';
import '../App.css';
import Map from '../container/MapContainer';
const VIEWS = {map: 'map', chart: 'chart'}

export default class Devices extends Component {
  state={
      view: 'chart'
  }
  render() {
    return (
      <div className="App-devices space">
        <h3> devices {this.state.view}</h3>
          <p>
              <FontAwesomeIcon className="icon" icon="chart-bar" onClick={()=> this.setState({view: VIEWS.chart}) }/>
              \
              <FontAwesomeIcon className="icon" icon="globe-americas" onClick={() => this.setState({view: VIEWS.map}) }/>
               </p>
            <div className="App-device-view">
                {this.state.view === VIEWS.chart? <Chart /> : <Map />}
            </div>
      </div>
    );
  }
}
