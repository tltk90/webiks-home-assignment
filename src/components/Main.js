import React, { Component } from 'react';
import DevicesList from '../container/FilterDeviceList';
import Devices from './Devices';
import '../App.css';

export default class Header extends Component {

    componentWillMount(){
        
        this.i = 0;
        this.getNewDevice = setInterval(() => {
            this.props.addDevice(this.props.removeDevice)
            this.i++
        }, 200)
    }
    render() {
        return (
            <div className='App-main'>
                <DevicesList/>
                <Devices />
                
            </div>
        )
    }
}
