import React, { Component } from 'react';
import '../App.css';
import { FILTERS } from '../actions';
import DeviceRow from '../container/DeviceRowContainer';


export default class DevicesList extends Component {

    render() {
        return (<div className="App-list space">
            <h3>device list</h3>
            <table>
                <thead>
                    <tr>
                        <th style={{width: '5%'}} onClick={() => this.props.sortTable(FILTERS.default)}>#</th>
                        <th style={{width: '55%'}} onClick={() => this.props.sortTable(FILTERS.ByName) }>device name</th>
                        <th style={{width: '20%'}} onClick={ () => this.props.sortTable(FILTERS.ByPriority)}>priority</th>
                        <th style={{width: '20%'}} onClick={ () => this.props.sortTable(FILTERS.ByTTL) }>ttl</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.devices && this.props.devices.map((device, index) => {
                        return (
                            <DeviceRow key={index} 
                            index={index}
                             device={device} />
                        )
                    })
                    }
                </tbody>
            </table>
        </div>

        )
    }
}
