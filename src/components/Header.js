import React , {Component} from 'react';
import '../App.css';

 export default class Header extends Component {
    
    onChange = (e) => {
        this.props.update(parseInt(e.target.value))
    }
    render(){
        return(
            <div className='App-header'>
                <h3 className="space" >max devices: </h3>
                <input className="space" type='number' min={1} value={this.props.maxDevices} onChange={this.onChange}/>
            </div>
        )
    }
}