import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
const COLOR = { 1: '#021335', 2: '#016d01', 3: '#f2c100', 4: '#c35a00', 5: '#950010' }
const Device = ({ color }) => <FontAwesomeIcon className="icon" icon="mobile-alt" color={color} size={"2x"} />;

export default class Map extends Component {
    static defaultProps = {
        center: {
            lat: 32.073368,
            lng: 34.792671
        },
        zoom: 11
    };

    render() {
        return (
            // Important! Always set the container height explicitly
            <div style={{ height: '300px', width: '90%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: "AIzaSyCfC4aw9aJBQmpZzgF3RHqWe_YCqEVIPmw" }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                >
                    {this.props.devices && this.props.devices.map(device => <Device
                        lat={device.coord.lat}
                        lng={device.coord.lng}
                        color={COLOR[device.priority]} />)}
                </GoogleMapReact>
            </div>
        );
    }
}