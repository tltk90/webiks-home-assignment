import {connect} from 'react-redux'
import Map from '../components/Map';

const mapStateToProps = state => ({
    devices: state.data.devices
})


export default connect(
    mapStateToProps,
)(Map)