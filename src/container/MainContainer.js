import {connect} from 'react-redux'
import {addDevice,removeDevice} from '../actions';
import Main from '../components/Main';

const mapDispatchToProps = dispatch => ({
    addDevice: (remover) => dispatch(addDevice(remover)),
    removeDevice: (name) => dispatch(removeDevice(name))
})


export default connect(
    null,
    mapDispatchToProps
)(Main)