import { connect } from 'react-redux';
import {changeTTL} from '../actions';

import DeviceRow from '../components/DeviceRow';

const mapDispatchToProps = dispatch => ({
    changeTTL: (name, ttl) => dispatch(changeTTL(name,ttl))
})

export default connect(
    null,
    mapDispatchToProps
)(DeviceRow)