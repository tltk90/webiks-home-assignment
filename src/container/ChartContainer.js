import { connect } from 'react-redux';
import Chart from '../components/Chart';

const makeDataset = devices => {
    let data = { labels: ["1", "2", "3", "4", "5"] }
    let dataSetData = [0, 0, 0, 0, 0]
    devices.forEach((device) => {
        dataSetData[device.priority - 1] += 1
    })
    data['datasets'] = [
        {
            label: "Device by Priority",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: dataSetData
        }
    ]
    return data;
}

const mapStateToProps = state => ({
    data: makeDataset(state.data.devices)
})


export default connect(
    mapStateToProps
)(Chart)