import { connect } from 'react-redux';
import {setFilter} from '../actions';
import DevicesList from '../components/DevicesList';
import { FILTERS } from '../actions';

const filterDevices = (devices, filter,s) => {
    switch (filter) {
        case FILTERS.default:
            return devices.sort((a, b) => a.id - b.id);
        case FILTERS.ByName:
            return devices.sort((a, b) => a.name.localeCompare(b.name));
        case FILTERS.ByTTL:
            return devices.sort((a, b) => a.TTL - b.TTL);
        case FILTERS.ByPriority:
            return devices.sort((a, b) => a.priority - b.priority)
        default:
            throw new Error("unknow filter: " + filter)
    }
}

const mapStateToProps = state => ({
    devices: filterDevices(state.data.devices, state.filters),
    filter: state.filters
})

const mapDispatchToProps = dispatch => ({
    sortTable: filter => dispatch(setFilter(filter))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DevicesList)