import {connect} from 'react-redux';
import {updateMaximum} from '../actions'
import Header from '../components/Header'

const mapStateToProps = state => ({
    maxDevices: state.data.max,
})

const mapDispatchToProps = dispatch => ({
    update: newMax => dispatch(updateMaximum(newMax))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header)